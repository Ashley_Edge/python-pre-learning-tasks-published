def vowel_swapper(string):
    # ==============
    # Your code here
    
    swap = string.replace("a", "4", 1).replace("A", "4", 1).replace("e", "3", 1).replace("E", "3", 1).replace("i", "!", 1).replace("I", "!", 1).replace("u", "|_|", 1).replace("U", "|_|", 1).replace("o", "ooo", 1).replace("O", "000", 1)
    
    return swap
    
            # so far I can only change the 1st instance of a given vowel --- "44a 33e !!i ooo000o |_||_|u" instead of "a4a e3e i!i o000o u|_|u"
    
    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu"))  # Should print "a4a e3e i!i o000o u|_|u" to the consol
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av4!lable" to the console
