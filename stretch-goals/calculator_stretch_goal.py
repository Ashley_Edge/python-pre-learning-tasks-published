def calculator(a, b, operator):
    # ==============
    # Your code here

    if operator == "+":
        result = bin(a + b) [2:]

    elif operator == "-":
        result = bin(a - b) [2:]

    elif operator == "*":
        result = bin(a * b) [2:]

    elif operator == "/":
        result = bin(a / b) [2:]

    return result

    # ==============

print(calculator(2, 4, "+")) # Should print 110 to the console
print(calculator(10, 3, "-")) # Should print 111 to the console
print(calculator(4, 7, "*")) # Should output 11100 to the console
print(calculator(100, 2, "/")) # Should print 110010 to the console
