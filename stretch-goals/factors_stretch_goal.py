def factors(number):
    # ==============
    # Your code here

    factors_list = []

    for i in range (2,number): 
        if number % i == 0: 
            factors_list.append(i)
       
    return str(number) + " is a prime number" if len(factors_list) == 0 else factors_list
    
    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print 13 is a prime number